;;;; -*- mode: common-lisp -*-
(declaim (optimize safety))

(defpackage "MOST-USED-WORDS"
  (:use "CL")
  (:export "LIST-MOST-USED-WORDS-STREAM"
	   "LIST-MOST-USED-WORDS-FILE"
	   "LIST-MOST-USED-WORDS-FILES"
	   "PRINT-MOST-USED-WORDS-FILE"
	   "PRINT-MOST-USED-WORDS-FILES"
	   "PRINT-MOST-UNIQUE-WORDS-FILE"
	   "PRINT-MOST-UNIQUE-WORDS-FILES"))
(in-package "MOST-USED-WORDS")


(defparameter *common-words*
  #("the"
    "and"
    "of"
    "to"
    "a"
    "in"
    "i"
    "that"
    "is"
    "be"
    "as"
    "or"
    "it"
    "with"
    "by"
    "this"
    "which"))
(setf *common-words* (sort *common-words* #'string<))

;; Auxiliary functions
(defun string-not-alphabetic-p (string)
  (find-if-not #'alpha-char-p string))

;; A word-counting structure
(defclass word-count ()
  ((word :accessor word :initarg :word)
   (wcount :accessor wcount :initarg :wcount)))

;; (defclass word-frequency ()
;;   ((word :accessor word :initarg :word)
;;    (frequency :accessor frequency :initarg :frequency)))

;; Workhorses
(defun order-words-by-count (counts &key comparator)
  "Return an array of the words in the hash table COUNTS, in order of frequency."
  (let ((sorted-counts (make-array (hash-table-count counts))))
    (loop for i below (hash-table-count counts)
       and word being the hash-keys of counts
       using (hash-value count)
       do (setf (aref sorted-counts i) (make-instance 'word-count :word word :wcount count)))
    (setf sorted-counts (sort sorted-counts comparator :key #'wcount))))


(defun list-words (string)
  "List the words in STRING separated by whitespace."
  (split-sequence:split-sequence-if #'bluewolf-utilities:whitespacep string))

(defun list-frequent-words (n counts)
  "List the N words most frequent by count in hash table COUNTS."
  (let ((sorted-counts (order-words-by-count counts :comparator #'>)))
    (loop for word-count across (subseq sorted-counts 0 n)
       collect (word word-count))))

(defun list-unique-words (n counts)
  "List the N words most unique by count in hash table COUNTS."
  (let ((sorted-counts (order-words-by-count counts :comparator #'<)))
    (loop for word-count across (subseq sorted-counts 0 n)
       collect (word word-count))))

(defun list-extremum-words-stream (&key stream)
  "Make a list of the N most used words in STREAM."
  (let* ((counts (make-hash-table :size 30000 :test #'equal))
	 (buffer-size (* 10 4096))
	 (buffer (make-array buffer-size :element-type (stream-element-type stream))))
    (flet ((count-words ()
	     (dolist (word (list-words buffer))
	       (let ((downword (string-downcase word)))
		 (unless (or (string-not-alphabetic-p downword)
			     (find downword *common-words* :test #'string=))
		   (incf (gethash downword counts 0)))))))
      (loop until (< (read-sequence buffer stream) buffer-size)
	 do (count-words)
	 finally (count-words)))
    counts))

(defun list-most-used-words-stream (&key n stream)
  (list-frequent-words n (list-extremum-words-stream :stream stream)))

(defun list-most-unique-words-stream (&key n stream)
  (list-unique-words n (list-extremum-words-stream :stream stream)))

;; (defun list-most-used-words-stream (&key n stream)
;;   "Make a list of the N most used words in STREAM."
;;   (let* ((counts (make-hash-table :size 30000 :test #'equal))
;; 	 (buffer-size (* 4 4096))
;; 	 (buffer (make-array buffer-size :element-type (stream-element-type stream))))
;;     (flet ((count-words ()
;; 	     (let* ((length (length buffer))
;; 		    (start 0)
;; 		    (end 0))
;; 	       (loop until (or (> end length)
;; 			       (> start length))
;; 		  do (let* ((word (subseq buffer start end))
;; 			    (downword (string-downcase word)))
;; 		       (setf start (or (position-if-not #'bluewolf-utilities:whitespacep buffer :start end)
;; 				       length))
;; 		       (setf end (or (position-if #'bluewolf-utilities:whitespacep buffer :start start)
;; 				     length))
;; 		       (incf end)
;; 		       (unless (find downword *common-words* :test #'string=)
;; 			 (incf (gethash downword counts 0))))))))
;;       (loop until (< (read-sequence buffer stream) buffer-size)
;; 	 do (count-words)
;; 	 finally (count-words)))
;;     (list-frequent-words n counts)))

(defun list-most-used-words-file (filename &key n)
  "Make a list of the N most used words in file named FILENAME."
  (with-open-file (stream filename
			  :direction :input)
    (list-most-used-words-stream :n n :stream stream)))

(defun list-most-unique-words-file (filename &key n)
  "Make a list of the N most unique words in file named FILENAME."
  (with-open-file (stream filename
			  :direction :input)
    (list-most-unique-words-stream :n n :stream stream)))

(defun list-most-used-words-files (filenames &key n)
  "Make a list of the N most used words over all the FILENAMES."
  (let* ((streams (mapcar #'open filenames))
	 (stream (apply #'make-concatenated-stream streams))
	 (most-used (list-most-used-words-stream :n n :stream stream)))
    (mapc #'close streams)
    most-used))

(defun list-most-unique-words-files (filenames &key n)
  "Make a list of the N most unique words over all the FILENAMES."
  (let* ((streams (mapcar #'open filenames))
	 (stream (apply #'make-concatenated-stream streams))
	 (most-used (list-most-unique-words-stream :n n :stream stream)))
    (mapc #'close streams)
    most-used))

;; Interfaces
(defun print-most-used-words-file (filename &key (n 10))
  (let ((most-used (list-most-used-words-file
		    filename
		    :n n)))
    (dolist (word most-used)
      (format t "~a~%" word))))

(defun print-most-used-words-files (filenames &key (n 10))
  (let ((most-used (list-most-used-words-files
		    filenames
		    :n n)))
    (dolist (word most-used)
      (format t "~a~%" word))))

(defun print-most-unique-words-file (filename &key (n 10))
  (let ((most-unique (list-most-unique-words-file
		      filename
		      :n n)))
    (dolist (word most-unique)
      (format t "~a~%" word))))

(defun print-most-unique-words-files (filenames &key (n 10))
  (let ((most-unique (list-most-unique-words-files
		      filenames
		      :n n)))
    (dolist (word most-unique)
      (format t "~a~%" word))))
