(defsystem :most-used-words
  :default-component-class cl-source-file.cl
  :description "List most frequently used words in text files."
  :version "0.0"
  :author "Udyant Wig <udyant.wig@gmail.com>"
  :licence "Undecided"
  :depends-on (:bluewolf-utilities :split-sequence)
  :components ((:file "most-used-words")))
